/**
 * @labelField       screen_name
 * @dataManagerGroup Twitter
 */

component {
	property name="screen_name"             type="string"  dbtype="varchar" maxlength=100 required=true uniqueindexes="twitterScreenName";
	property name="consumer_api_key"        type="string"  dbtype="varchar" maxlength=100 required=true;
	property name="consumer_api_secret_key" type="string"  dbtype="varchar" maxlength=100 required=true;
	property name="access_token"            type="string"  dbtype="varchar" maxlength=100 required=true;
	property name="access_token_secret"     type="string"  dbtype="varchar" maxlength=100 required=true;
	property name="active"                  type="boolean" dbtype="boolean" default=true;
	property name="timeline_start_date"     type="date"    dbtype="date";
}
