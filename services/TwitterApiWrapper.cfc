/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	/**
	 * @utils.inject TwitterUtils
	 */
	public any function init( required any utils ) {
		_setUtils( arguments.utils );
		return this;
	}

//PUBLIC METHODS
	/**
	 * Makes a call to the statuses/user_timeline endpoint:
	 * https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
	 *
	 * Returns a TwitterRequest object which will contain the success status and the returned data.
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function getUserTimeline( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "GET"
			, endpoint           = "statuses/user_timeline.json"
		);
	}

	/**
	 * Makes a call to the statuses/lookup endpoint:
	 * https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-lookup
	 *
	 * Returns a TwitterRequest object which will contain the success status and the returned data.
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function getStatuses( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "GET"
			, endpoint           = "statuses/lookup.json"
		);
	}

	/**
	 * Makes a call to the statuses/update endpoint:
	 * https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/post-statuses-update
	 *
	 * Posts a new status update (Tweet).
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function postStatus( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "POST"
			, endpoint           = "statuses/update.json"
		);
	}

 // GETTERS AND SETTERS
	private any function _getUtils() {
		return _utils;
	}
	private void function _setUtils( required any utils ) {
		_utils = arguments.Utils;
	}

}